﻿package aufgaben;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double rückgabebetrag;
       char weiter = 'y';
       while (weiter == 'y') {
		
	
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

       eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
       
       fahrkartenAusgeben();
       
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       rueckgeldAusgeben(rückgabebetrag);
       

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       System.out.println("weiter? [y/n]");
       weiter = tastatur.next().charAt(0);
       
    }
    }
	
		
	
	private static void rueckgeldAusgeben(double rückgabebetrag) {
		 if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
		
	}

	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		
	}

	private static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen:%.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2.00 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		// TODO Auto-generated method stub
		return eingezahlterGesamtbetrag;
	}

	private static double fahrkartenbestellungErfassen(Scanner tastatur) {
		int anzahlDerKarten = 0;
		double zuZahlenderBetrag = 0;
		int typDerKarte;
		
		String[] allKartenTypenArray = {
	            "Einzelfahrschein Berlin AB (1)"
	           , "Einzelfahrschein Berlin BC (2)"
	           , "Einzelfahrschein Berlin ABC (3)"
	           , "Kurzstrecke (4)"
	           , "Tageskarte Berlin AB (5)"
	           , "Tageskarte Berlin BC (6)"
	           , "Tageskarte Berlin ABC (7)"
	           , "Kleingruppen-Tageskarte Berlin AB (8)"
	           , "Kleingruppen-Tageskarte Berlin BC (9)"
	           , "Kleingruppen-Tageskarte Berlin ABC (10)"};
					double[] typDerKarteArray = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
					System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
					for (int i = 0; i < typDerKarteArray.length; i++) {
	System.out.println(allKartenTypenArray[i] + " - " + typDerKarteArray[i] + "€" );
	
}
		typDerKarte = tastatur.nextInt();
		zuZahlenderBetrag = typDerKarteArray[typDerKarte - 1 ];
			
		if (typDerKarte > typDerKarteArray.length || typDerKarte < 1) {
			System.out.println("Falsche Eingabe");
	}
		
		System.out.println("Ihre Wahl:" + typDerKarte);
	       System.out.print("Anzahl der Fahrkarten: ");
	       anzahlDerKarten = tastatur.nextInt();
	       if (anzahlDerKarten > 10 || anzahlDerKarten < 1) {
	    	   System.out.println("Ungültige Eingabe!");
	    	   anzahlDerKarten = 1;		
		}
	       zuZahlenderBetrag = (double)anzahlDerKarten * zuZahlenderBetrag;
	       return zuZahlenderBetrag;		
	}
}