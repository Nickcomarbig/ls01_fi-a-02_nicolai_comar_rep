package test03;

public class Aufgabe02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.printf ("%5s%8s%19s%4d\n","0!","=","=",1 );
		System.out.printf ("%5s%8s%2s%17s%4d\n", "1!", "=","1", "=", 1 );
		System.out.printf ("%5s%8s%2s%2s%2s%13s%4d\n","2!","=", "1" , "*", "2", "=", 2);
		System.out.printf ("%5s%8s%2s%2s%2s%2s%2s%9s%4d\n", "3!", "=", "1" , "*", "2", "*", "3", "=", 6);
		System.out.printf ("%5s%8s%2s%2s%2s%2s%2s%2s%2s%5s%4d\n", "4!", "=", "1" , "*", "2", "*", "3", "*", "4", "=", 24);
		System.out.printf ("%5s%8s%2s%2s%2s%2s%2s%2s%2s%2s%2s%1s%4d\n", "4!", "=", "1" , "*", "2", "*", "3", "*", "4", "*", "5", "=", 120);
	}

}
