package aufgaben;

public class countingLoops {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		fourLoopsInAMethod();
		
	}

	private static void fourLoopsInAMethod() {
		int x = 0;
		while (x<10) {
			x++;
			System.out.print(x);
		}
		int y = 10;
		System.out.println();
		while (y>0) {
			y--;
			System.out.print(y);
			
		}
		System.out.println();
		for (int a = 0; a < 10; a++) {
			System.out.print(a);
			
		}
		System.out.println();
		for (int b = 10; b > 0; b--) {
			System.out.print(b);
			
		}
		// TODO Auto-generated method stub
		
	}

}
