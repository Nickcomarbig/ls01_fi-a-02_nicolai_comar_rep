package aufgaben;
import java.util.Scanner;
public class zahlenFolgen {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		sumAWhile (keyboard.nextInt());
		sumBWhile (keyboard.nextInt());
		sumCFor(keyboard.nextInt());
		sumDfor(keyboard.nextInt());
		
	}

	private static void sumDfor(int n) {
		int sum = 0;
		for (int x = 0; x < n; x++) {
			if (sum!=0) {
				System.out.print((-2*x)+"+");
			}
			System.out.print((x*2+1));
			sum+=(2*x+1)-(2*x);
		}	
		System.out.print("=" + sum);
		System.out.println();
		System.out.println("sum:" + sum);
		System.out.println("grenzwert:" + n);
		// TODO Auto-generated method stub
		
	}

	private static void sumCFor(int n) {
		int sum = 0;
		for (int x = 0; x < n; x++) {
			if (sum!=0) {
				System.out.print("+");
			}
			System.out.print(2*x+1);
			sum+=2*x+1;
			
		}
		System.out.print("=" + sum);
		System.out.println();
		System.out.println("sum:" + sum);
		System.out.println("grenzwert:" + n);
		// TODO Auto-generated method stub
		
	}

	private static void sumBWhile(int n) {
		int x = 0;
		int sum = 0;
		while (x<n) {
			x++;
			
			if (sum!=0) {
				System.out.print("+");
			}
			System.out.print(2*x);
			sum+=2*x;
			
		}
		System.out.print("=" + sum);
		System.out.println();
		System.out.println("sum:" + sum);
		System.out.println("grenzwert:" + n);
		// TODO Auto-generated method stub
		
	}

	private static void sumAWhile(int n) {
		int x = 0;
		int sum = 0;
		while (x<n) {
			x++;
			
			if (sum!=0) {
				System.out.print("+");
			}
			System.out.print(x);
			sum+=x;
			
		}
		System.out.print("=" + sum);
		System.out.println();
		System.out.println("sum:" + sum);
		System.out.println("grenzwert:" + n);
		
		
		// TODO Auto-generated method stub
		
	}

}
